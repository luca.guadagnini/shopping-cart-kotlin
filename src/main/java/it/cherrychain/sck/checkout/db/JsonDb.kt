package it.cherrychain.sck.checkout.db

import io.vertx.core.json.JsonObject

interface Repository<T> : Iterable<T> {
  fun add(t: T): Boolean
  fun edit(t: T): Boolean
  fun remove(t: T): Boolean
}

interface JsonDb : Repository<JsonObject>

fun jsonDb(): JsonDb = JsonDbImpl()

private class JsonDbImpl(db: Array<JsonObject> = arrayOf()) : JsonDb {
  private var db = db

  override fun edit(json: JsonObject) =
    db.indexOfFirst { it.getString("id") == json.getString("id") }
      .also { db[it] = json }
      .let { it >= 0 }

  override fun remove(json: JsonObject) =
    db.indexOfFirst { it.getString("id") == json.getString("id") }
      .let { db.filter { item -> item != db[it] } }
      .let { it.toTypedArray() }
      .also { db = it }
      .let { true }

  override fun add(json: JsonObject): Boolean {
    db += json
    return true
  }

  override fun iterator() =
    db
      .sortedBy { it.getString("id") }
      .distinctBy { it.getString("id") }
      .iterator()
}