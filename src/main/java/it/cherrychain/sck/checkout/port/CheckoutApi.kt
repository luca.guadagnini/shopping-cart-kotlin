package it.cherrychain.sck.checkout.port

import io.vertx.core.Vertx
import io.vertx.core.eventbus.EventBus
import io.vertx.core.json.JsonArray
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import it.cherrychain.sck.checkout.db.JsonDb
import it.cherrychain.sck.checkout.db.jsonDb

typealias Endpoint = () -> Unit

fun checkoutApi(vertx: Vertx, router: Router) = router
  .let { Router.router(vertx) }
  .also { getCheckout(it)() }
  .also { postCheckout(vertx, it) }
  .let { router.mountSubRouter("/api", it) }

fun getCheckout(router: Router): Endpoint = GetCheckouts(router)
fun postCheckout(vertx: Vertx, router: Router): Endpoint = PostCheckout(router, vertx.eventBus())

class GetCheckouts(router: Router, memdb: JsonDb = jsonDb()) : Endpoint {
  private val router = router
  private val memdb = memdb

  override fun invoke() {
    router.get("/checkouts").handler {
      it.response().end(JsonArray(memdb.toList()).toBuffer())
    }
  }
}

class PostCheckout(router: Router, eventBus: EventBus) : Endpoint {
  private val router = router
  private val eventBus = eventBus

  override fun invoke() {
    router.post("/checkouts")
      .consumes("application/json")
      .handler(BodyHandler.create())
      .handler { context ->
        context
          .also { eventBus.publish("checkout.commands", context.bodyAsJson) }
          .response()
          .end()
      }
      .failureHandler { context ->
        context
          .response()
          .setStatusCode(400)
          .end("""Can't process del body ${context.bodyAsString} ${context.failure().message}.""")
      }
  }
}