package it.cherrychain.sck.checkout.port

import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.StaticHandler

fun webroot(router: Router) = router.route("/*").handler(StaticHandler.create())